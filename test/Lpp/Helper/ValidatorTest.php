<?php
/**
 * Created by PhpStorm.
 * User: seba
 * Date: 2019-06-20
 * Time: 17:27
 */

namespace Lpp\Helper;

use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{

    public function testUrl()
    {
        $validator = new Validator();
        $this->assertTrue($validator->url('http://www.example.com'));
        $this->assertTrue($validator->url('http://example.com'));
        $this->assertTrue($validator->url('https://www.example.com'));
        $this->assertTrue($validator->url('http://www.example.com/'));
        $this->assertFalse($validator->url('www.example.com'));
        $this->assertFalse($validator->url('http:/www.example.com'));
    }
}
