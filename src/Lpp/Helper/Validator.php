<?php
/**
 * Created by PhpStorm.
 * User: seba
 * Date: 2019-06-20
 * Time: 17:23
 */

namespace Lpp\Helper;

/**
 * Class Validator.
 * Simple data validation helper class.
 * @package Lpp\Helper
 */
class Validator
{
    /**
     * @param $string
     * @return bool
     */
    public function url($string)
    {
        return filter_var($string, FILTER_VALIDATE_URL) ? true : false;
    }
}