<?php
/**
 * Created by PhpStorm.
 * User: seba
 * Date: 2019-06-20
 * Time: 16:28
 */

namespace Lpp\Helper\Collection;


interface CollectionInterface
{
    /**
     * Get collection items.
     * @param null $key If key is specified, it returns only this one item from collection.
     * @return mixed
     */
    public function getItem($key = null);

    /**
     * Add new item into collection.
     * @param $obj
     * @return mixed Key of last inserted object into collection.
     */
    public function addItem($obj);

    /**
     * Remove item from collection.
     * @param mixed $key The key of array item.
     * @return void
     */
    public function removeItem($key);
}