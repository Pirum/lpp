<?php
/**
 * Created by PhpStorm.
 * User: seba
 * Date: 2019-06-20
 * Time: 16:19
 */

namespace Lpp\Helper\Collection;


class AbstractCollection implements CollectionInterface
{
    protected $item = array();

    /**
     * @param null $key
     * @return array
     */
    public function getItem($key = null): array
    {
        return is_null($key) ? $this->item : $this->item[$key];
    }

    /**
     * @param $obj
     * @return mixed
     */
    public function addItem($obj)
    {
        return array_push($this->item, $obj);
    }

    /**
     * @param mixed $key
     */
    public function removeItem($key): void
    {
        unset($this->item[$key]);
    }
}