<?php
namespace Lpp\Entity;


use Lpp\Helper\Collection\PriceCollection;

/**
 * Represents a single item from a search result.
 * 
 */
class Item
{
    /**
     * Name of the item.
     *
     * @var string
     */
    protected $name;

    /**
     * Url of the item's page.
     * 
     * @var string
     */
    protected $url;

    /**
     * Unsorted list of prices received from the 
     * actual search query.
     * 
     * @var array
     */
    protected $price;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Item
     */
    public function setName(string $name): Item
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Item
     */
    public function setUrl(string $url): Item
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return PriceCollection
     */
    public function getPrice(): PriceCollection
    {
        return $this->price;
    }

    /**
     * @param array $price
     * @return Item
     */
    public function setPrice(array $price): Item
    {
        $this->price = $price;

        return $this;
    }

}
