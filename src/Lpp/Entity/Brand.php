<?php
namespace Lpp\Entity;

/**
 * Represents a single brand in the result.
 *
 */
class Brand
{
    /**
     * Name of the brand.
     *
     * @var string
     */
    protected $name;

    /**
     * Brand's description.
     * 
     * @var string
     */
    protected $description;

    /**
     * Unsorted list of items with their corresponding prices.
     * 
     * @var array
     */
    protected $item;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Brand
     */
    public function setName(string $name): Brand
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Brand
     */
    public function setDescription(string $description): Brand
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array
     */
    public function getItem(): array
    {
        return $this->item;
    }

    /**
     * @param array $item
     * @return Brand
     */
    public function setItem(array $item): Brand
    {
        $this->item = $item;

        return $this;
    }


}
