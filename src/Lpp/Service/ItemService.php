<?php
/**
 * Created by PhpStorm.
 * User: seba
 * Date: 2019-06-20
 * Time: 14:13
 */

namespace Lpp\Service;


use Lpp\Entity\Brand;
use Lpp\Entity\Item;
use Lpp\Entity\Price;
use Lpp\Helper\Collection\BrandCollection;
use Lpp\Helper\Collection\ItemCollection;
use Lpp\Helper\Collection\PriceCollection;
use Lpp\Helper\Validator;

class ItemService implements ItemServiceInterface
{
    /**
     * Data read from datasource.
     * @var \stdClass
     */
    protected $data;

    /**
     * Collection of Brand objects.
     * @var BrandCollection
     */
    protected $brandCollection;

    /**
     * Collection of Item objects.
     * @var ItemCollection
     */
    protected $itemCollection;

    /**
     * Collection of Price objects.
     * @var PriceCollection
     */
    protected $priceCollection;

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var array
     */
    public $error = array();

    /**
     * ItemService constructor.
     */
    public function __construct()
    {
        $this->brandCollection = new BrandCollection();
        $this->itemCollection = new ItemCollection();
        $this->priceCollection = new PriceCollection();
        $this->validator = new Validator();
    }

    /**
     * @return \stdClass
     */
    public function getData(): \stdClass
    {
        return $this->data;
    }


    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @return array|bool|object|\stdClass
     * @throws \Exception
     */
    public function getResultForCollectionId($collectionId)
    {
        try {
            $this->readFromJsonDatasource(ROOT_DIR . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . $collectionId . '.json');
        } catch (\Exception $e) {
            array_push($this->error, $e->getMessage());

            return false;
        }

        foreach ($this->data->brands as $brandItem) {
            $brand = new Brand();
            $brand->setName($brandItem->name);

            if (property_exists($brandItem, 'description')) {
                $brand->setDescription($brandItem->description);
            }

            if (property_exists($brandItem, 'items')) {
                foreach ($brandItem->items as $items) {
                    $item = new Item();
                    $item->setName($items->name);
                    if (property_exists($items, 'url') && $this->validator->url($items->url)) {
                        $item->setUrl($items->url);
                    }

                    if (property_exists($items, 'prices')) {
                        foreach ($items->prices as $prices) {
                            $price = new Price();
                            $price->setDescription($prices->description);
                            $price->setArrivalDate(new \DateTime($prices->arrival));
                            $price->setDueDate(new \DateTime($prices->due));
                            $price->setPriceInEuro($prices->priceInEuro);

                            $this->priceCollection->addItem($price);
                        }
                    }
                    $item->setPrice($this->priceCollection->getItem());

                    $this->itemCollection->addItem($item);
                }

            }
            $brand->setItem($this->itemCollection->getItem());

            $this->brandCollection->addItem($brand);
        }

        $data = $this->brandCollection->getItem();

        return $data;
    }

    /**
     * Read data from datasource.
     * @param $filepath
     * @return bool
     * @throws \Exception
     */
    private function readFromJsonDatasource($filepath)
    {
        if ($data = json_decode(file_get_contents($filepath))) {
            $this->data = $data;

            return true;
        }

        throw new \Exception('Unable to get contents from file');
    }
}